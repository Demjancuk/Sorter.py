from os.path import exists

def get_bool(inputText):
    while True:
        boolVar = input(inputText)
        if boolVar == "0":
            return False
        elif boolVar == "1":
            return True
        else:
            print("[WARN] " + "\""+ boolVar + "\"" + " was not recognized as '0' or '1'. Enter '0'(false) or '1'(true) !")

def get_path():
    while True:
        folderPath = input("Enter folder path to sort : ")
        if exists(folderPath):
            return folderPath
        else:
            print("[WARN] " + "Folder does not exists !")

def removeLastSubstring(string, removeable):
  if string.endswith(removeable):
    return string[:-len(removeable)]
  return string
