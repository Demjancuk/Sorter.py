from TypeSorter import TypeSorter
from Commands import get_bool, get_path
from NameSorter import NameSorter

folderPath = get_path()

if get_bool("Do you want to sort folder files by type ? (0/1) "):
    typeSorter = TypeSorter(folderPath)
    typeSorter.loadDataFile()
    typeSorter.createFileForEachType()
    typeSorter.moveFilesToDirs()

elif get_bool("Want you want to sort folder files by name ? (0/1) "):
    nameSorter = NameSorter(folderPath)
    nameSorter.loadDataFile()
    nameSorter.sortFiles()
