class File(object):
    
    def __init__(self):
        self.folderPath = ""
        self.fileName = ""
        self.fileType = ""
        self.fileSize = -1

    def setFolderPath(self, _folderPath):
        self.folderPath = _folderPath

    def setFileName(self, _fileName):
        self.fileName = _fileName
        
    def setFileType(self, _fileType):
        self.fileType = _fileType

    def setFileSize(self, _fileSize):
        self.fileSize = _fileSize




