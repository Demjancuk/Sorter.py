from os.path import isfile, join, exists
from os import rename, makedirs, listdir
from File import File

class NameSorter(object):

    def __init__(self, _folderPath):
        self.folderPath = _folderPath
        self.files = []

    def loadDataFile(self):
        fileNames = [f for f in listdir(self.folderPath) if isfile(join(self.folderPath, f))]

        for fileName in fileNames:
            file = File()
            file.setFileName(fileName)
            file.setFolderPath(self.folderPath)
            self.files.append(file)

    def sortFiles(self):
        for file in self.files:
            if not exists(file.folderPath + "\\" + file.fileName[0]):
                makedirs(file.folderPath + "\\" + file.fileName[0])
            beforeMoveNameWithDir = file.folderPath + "\\" + file.fileName
            afterMoveNameWithDir = file.folderPath + "\\" + file.fileName[0] + "\\" + file.fileName
            rename(beforeMoveNameWithDir,afterMoveNameWithDir)
            print("[INFO] " + beforeMoveNameWithDir + " successfully moved!")
            